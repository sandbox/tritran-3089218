<?php

namespace Drupal\commerce_alepay\Plugin\Commerce\PaymentType;

use Drupal\commerce_payment\Plugin\Commerce\PaymentType\PaymentTypeBase;

/**
 * Provides the Alepay payment type.
 *
 * @CommercePaymentType(
 *   id = "alepay_payment",
 *   label = @Translation("Alepay"),
 * )
 */
class AlepayPayment extends PaymentTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    return [];
  }

}
