<?php

namespace Drupal\commerce_alepay\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_alepay\Entity\AlepayLog;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\commerce_alepay\Entity\Alepay;

/**
 * Provides All In One Pay Gate  for Alepay.
 *
 * @CommercePaymentGateway(
 *    id = "alepay_paygate",
 *    label = @Translation("Alepay (All in One)"),
 *    display_label = @Translation("Alepay (All in One)"),
 *    forms = {
 *      "offsite-payment" =
 *   "Drupal\commerce_alepay\PluginForm\AllInOnePayGateForm",
 *    },
 *    payment_type = "alepay_payment",
 * )
 */
class AllInOnePayment extends OffsitePaymentGatewayBase {

  /**
   * Drupal\commerce_alepay\Entity\Alepay.
   *
   * @var \Drupal\commerce_alepay\Entity\Alepay
   */
  protected $alepay;

  /**
   * Drupal\Core\Logger\LoggerChannel.
   *
   * @var \Drupal\Core\Logger\LoggerChannel
   */
  protected $logger;

  protected $messenger;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    PaymentTypeManager $payment_type_manager,
    PaymentMethodTypeManager $payment_method_type_manager,
    TimeInterface $time,
    Alepay $aio_pay_gate,
    LoggerChannelInterface $logger,
    MessengerInterface $messenger
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $entity_type_manager,
      $payment_type_manager,
      $payment_method_type_manager,
      $time
    );

    $this->alepay = $aio_pay_gate;
    $this->logger = $logger;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('commerce_alepay.alepay_aio'),
      $container->get('logger.channel.commerce_alepay'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $configuration = [
      'token_key' => '',
      'checksum_key' => '',
      'encrypt_key' => '',
      'redirect_method' => 'get',
    ];
    return $configuration + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['token_key'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Token key'),
      '#default_value' => $this->configuration['token_key'] ? $this->configuration['token_key'] : '',
    ];
    $form['checksum_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Checksum key'),
      '#default_value' => $this->configuration['checksum_key'] ? $this->configuration['checksum_key'] : '',
      '#required' => TRUE,
    ];
    $form['encrypt_key'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Encrypt key'),
      '#default_value' => $this->configuration['encrypt_key'] ? $this->configuration['encrypt_key'] : '',
      '#required' => TRUE,
    ];
    $form['redirect_method'] = [
      '#type' => 'radios',
      '#title' => $this->t('Redirect method'),
      '#options' => [
        'get' => $this->t('Redirect via GET (automatic)'),
        'get_manual' => $this->t('Redirect via GET (manual)'),
      ],
      '#default_value' => $this->configuration['redirect_method'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['token_key'] = $values['token_key'];
      $this->configuration['checksum_key'] = $values['checksum_key'];
      $this->configuration['encrypt_key'] = $values['encrypt_key'];
      $this->configuration['redirect_method'] = $values['redirect_method'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $data_response = $request->query->all();
    if (empty($data_response['data'])) {
      throw new PaymentGatewayException('Return: data missing for this Alepay Checkout transaction.');
    }

    $data_decrypt = $this->alepay->decryptCallbackData($data_response['data'], $this->configuration);
    if (empty($data_decrypt['data'])) {
      throw new PaymentGatewayException('Return: data missing for this Alepay Checkout decryption.');
    }

    // Return json_string;.
    $transaction_info = $this->alepay->getTransactionInfo($data_decrypt['data']);
    \Drupal::logger('commerce_alepay')
      ->info('onReturn() 4-INFO: ' . $transaction_info);


    // $transaction_info: data array after request to Alepay to get Transaction info
    $transaction_info = json_decode($transaction_info, TRUE);
    if (isset($transaction_info['transactionCode']) && isset($transaction_info['orderCode'])) {

      // Write log file.
      $alepay_log = new AlepayLog();
      $alepay_log->writeTransactionToFile($transaction_info);

      try {
        /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
        $payments = $this->entityTypeManager->getStorage('commerce_payment')
          ->loadByProperties(['order_id' => $order->id()]);
        $payment = reset($payments);

        $payment->set('state', 'authorization');
        $payment->set('amount', $order->getTotalPrice());
        $payment->set('payment_gateway', $this->entityId);
        $payment->set('remote_id', $transaction_info['transactionCode']);
        $payment->set('remote_state', $transaction_info['message']);
        $payment->set('authorized', $this->time->getRequestTime());

        try {
          $payment->save();
          $this->logger->log('notice', 'Payment entity with id ' . $payment->id() . ' has been updated.');
        } catch (\Exception $ex) {
          $this->messenger->addError($this->t('Cannot update process payment.'));
          throw new PaymentGatewayException($this->t('Cannot update process payment.'));
        }
      } catch (\Exception $ex) {
        $this->messenger->addError($this->t('Payment entity not found.'));
        throw new PaymentGatewayException($ex->getMessage());
      }
    }
    else {
      \Drupal::logger('commerce_alepay')
        ->info('onReturn() 4.1-: Transaction info not exist transactionCode or orderCode');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onCancel(OrderInterface $order, Request $request) {
    $this->messenger
      ->addError($this->t('You have canceled checkout at @gateway but may resume the checkout process here when you are ready.', [
        '@gateway' => $this->getDisplayLabel(),
      ]), 'error');
  }

  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request) {
    $data_response = $request->query->all();
    if (empty($data_response['data'])) {
      throw new PaymentGatewayException('Notify: data missing for this Alepay Checkout transaction.');
    }

    $data_decrypt = $this->alepay->decryptCallbackData($data_response['data'], $this->configuration);
    if (empty($data_decrypt['data'])) {
      throw new PaymentGatewayException('Notify: data missing for this Alepay Checkout decryption.');
    }

    // Return json_string;.
    $transaction_info = $this->alepay->getTransactionInfo($data_decrypt['data']);
    \Drupal::logger('commerce_alepay')
      ->info('onNotify() 5-INFO: ' . $transaction_info);

    $transaction_info = json_decode($transaction_info, TRUE);
    if (isset($transaction_info['transactionCode']) && isset($transaction_info['orderCode'])) {
      try {
        /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
        $payments = $this->entityTypeManager->getStorage('commerce_payment')
          ->loadByProperties(['order_id' => $transaction_info['orderCode']]);
        $payment = reset($payments);

        $payment->set('remote_id', $transaction_info['transactionCode']);
        $payment->set('remote_state', $transaction_info['message']);
        $payment->setState('completed');

        try {
          $payment->save();
          $this->logger->log('notice', 'Payment entity with id ' . $payment->id() . ' has been updated.');
        } catch (\Exception $ex) {
          $this->messenger->addError($this->t('Cannot update process payment.'));
          throw new PaymentGatewayException($this->t('Cannot update process payment.'));
        }
      } catch (\Exception $ex) {
        $this->messenger->addError($this->t('Payment entity not found.'));
        throw new PaymentGatewayException($ex->getMessage());
      }
    }
    else {
      \Drupal::logger('commerce_alepay')
        ->info('onNotify() 5.1-: Transaction info not exist transactionCode or orderCode');
    }

    return new JsonResponse($transaction_info, 200, [
      'Content-Type' => 'application/json',
      'charset' => 'UTF-8',
    ]);
  }

}
