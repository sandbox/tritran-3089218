<?php
/**
 * Created by PhpStorm.
 * User: TRI_TRAN
 * Date: 08-Oct-19
 * Time: 6:14 AM
 */

namespace Drupal\commerce_alepay\Plugin\Block;

use Drupal\commerce_alepay\Entity\AlepayLog;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'AlepayTransactionLog' Block.
 *
 * @Block(
 *   id = "alepay_transaction_log_block",
 *   admin_label = @Translation("Alepay transaction log block"),
 *   category = @Translation("Alepay transaction log block"),
 * )
 */
class AlepayTransactionLogBlock extends BlockBase
{
  /**
   * {@inheritdoc}
   */
  public function build()
  {
    $current_path = \Drupal::service('path.current')->getPath();
    $params = explode('/', $current_path);
    $order_id = end($params);
    $alepay_log = new AlepayLog();
    $data = $alepay_log->getContentTransactionLog($order_id);

    // Empty result if file not exist
    if ($data) {
      $date_formatter = \Drupal::service('date.formatter');
      $success_time = ($data['successTime'] != '0')? $date_formatter->format(
        $data['successTime'], 'custom', 'd/m/Y H:i:s'): "N/A";
      $transaction_time = $date_formatter->format(
        $data['transactionTime'], 'custom', 'd/m/Y H:i:s'
      );
      $build_params = [
        '#markup' => $this->t('Alepay Transaction Log'),
        '#theme' => 'commerce_alepay_transaction_log',
        '#title' => "Alepay Transaction Log",
        '#transactionCode' => $data['transactionCode'],
        '#orderCode' => $data['orderCode'],
        '#message' => $data['message'],
        '#bankCode' => $data['bankCode'],
        '#bankName' => $data['bankName'],
        '#method' => $data['method'],
        '#transactionTime' => $transaction_time,
        '#successTime' => $success_time,
        '#reason' => $data['reason']
      ];
      return $build_params;
    }
    return [];
  }
}