<?php
/**
 * Created by PhpStorm.
 * User: TRI_TRAN
 * Date: 29-Sep-19
 * Time: 10:51 PM
 */

namespace Drupal\commerce_alepay;


class Unit
{
  private $length = 0;
  /**
   * @param int $length
   */
  public function setLength(int $length) {
    $this->length = $length;
  }
  /**
   * @return int
   *   The length of the unit.
   */
  public function getLength() {
    return $this->length;
  }
}