<?php

namespace Drupal\commerce_alepay\Entity;


use Drupal\commerce_alepay\Entity\AlepayUtils;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\Core\Config\ConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;
use GuzzleHttp\ClientInterface;

class Alepay
{
  protected $alepayUtils;
  protected $publicKey = "";
  protected $checksumKey = "";
  protected $apiKey = "";
  protected $callbackUrl = "";
  protected $env = "live";
  protected $baseURL = array(
    'dev' => 'http://edigi.local.com',
    'test' => 'https://alepay-sandbox.nganluong.vn',
    'live' => 'https://alepay.vn'
  );

  /**
   * Configuration of payment gateway.
   *
   * @var array
   */
  protected $configuration;

  /**
   * Drupal\commerce_payment\Entity\PaymentInterface.
   *
   * @var \Drupal\commerce_payment\Entity\PaymentInterface
   */
  protected $payment;

  /**
   * Drupal\commerce_order\Entity\OrderInterface.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\PaymentGatewayInterface.
   *
   * @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\PaymentGatewayInterface
   */
  protected $paymentGateWayPlugin;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * POST <BASE_URL>/checkout/v1/get-transaction-history
   * API cho phép tìm kiếm thông tin giao dịch
   */

  protected $URI = array(
    'requestPayment' => '/checkout/v1/request-order?lang=eng', //
    'calculateFee' => '/checkout/v1/calculate-fee',
    'getTransactionInfo' => '/checkout/v1/get-transaction-info', //
    'requestCardLink' => '/checkout/v1/request-profile',
    'tokenizationPayment' => '/checkout/v1/request-tokenization-payment',
    'tokenizationPaymentDomestic' => '/checkout/v1/request-tokenization-payment-domestic',
    'cancelCardLink' => '/checkout/v1/cancel-profile',
    'requestCardLinkDomestic' => '/alepay-card-domestic/request-profile',
  );

  /**
   * Constructs a new AllInOneService object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Drupal\Core\Entity\EntityTypeManagerInterface.
   * @param \Drupal\Core\Config\ConfigManagerInterface $config_manager
   *   Drupal\Core\Config\ConfigManagerInterface.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   GuzzleHttp\ClientInterface.
   */

  public function __construct(EntityTypeManagerInterface $entity_type_manager,
                              ConfigManagerInterface $config_manager,
                              ClientInterface $http_client)
  {
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Credentials: true");
    header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
    header('Access-Control-Max-Age: 1000');
    header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

    $this->alepayUtils = new AlepayUtils();
    $this->entityTypeManager = $entity_type_manager;
    $this->configManager = $config_manager;
    $this->httpClient = $http_client;
  }

  private function setConnectionCodePayment() {
    $this->publicKey = trim($this->configuration['encrypt_key']);
    $this->checksumKey = trim($this->configuration['checksum_key']);
    $this->apiKey = trim($this->configuration['token_key']);
    $this->env = $this->configuration['mode'];
  }

  private function sendRequestToAlepay($data, $url)
  {
    $dataEncrypt = $this->alepayUtils->encryptData(json_encode($data), $this->publicKey);
    $checksum = md5($dataEncrypt . $this->checksumKey);
    $items = array(
      'token' => $this->apiKey,
      'data' => $dataEncrypt,
      'checksum' => $checksum
    );
    $data_string = json_encode($items);
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json',
        'Content-Length: ' . strlen($data_string))
    );
    $result = curl_exec($ch);
    // echo $result;
    \Drupal::logger('commerce_alepay')->info('sendRequestToAlepay() 1-RESULTS: ' . json_encode($result));
    return json_decode($result);
  }

  public function setPaymentGateWay(PaymentInterface $payment)
  {
    $this->payment = $payment;
    $this->paymentGateWayPlugin = $payment->getPaymentGateway()->getPlugin();
    $this->configuration = $this->paymentGateWayPlugin->getConfiguration();
    $this->order = $payment->getOrder();
  }

  /**
   * Collect data order to send to api Alepay
   * @return array
   */
  public function captureAlepayWallet($error_redirect_url)
  {
//    dump($this->order);
//    dump($this->order->getBillingProfile());
//    dump($this->order->getBillingProfile()->address->first());
//    dump($this->order->getBillingProfile()->get('field_full_name')->value);

    $this->setConnectionCodePayment();
    $host = \Drupal::request()->getSchemeAndHttpHost();
    $data['cancelUrl'] = $host . $error_redirect_url;
    $data['orderCode'] = $this->order->id();
    $data['amount'] = (double)$this->order->getTotalprice()->getNumber();
    $data['currency'] = $this->order->getTotalprice()->getCurrencyCode();
    $data['orderDescription'] = 'Custome order';
    $data['totalItem'] = count($this->order->getItems());
    $data['checkoutType'] = 1; // 0 : cho phép thanh tán bằng cả 2 cách, 1 : chỉ thanh toán thường , 2: chỉ thanh toán trả góp
    $data['buyerEmail'] = $this->order->getEmail();
    $data['paymentHours'] = '12'; // 12 tiếng :  Thời gian cho phép thanh toán (tính bằng giờ)

    // Case: default information
    $address = $this->order->getBillingProfile()->address->first();
    if ($address) {
      $customer_name = $address->getFamilyName() . " " . $address->getGivenName();
      $data['buyerPhone'] = ''; // TODO: get phone
      $data['buyerName'] = trim($customer_name);
      $data['buyerAddress'] = $address->getAddressLine1();
      $data['buyerCity'] = $address->getLocality();
      $data['buyerCountry'] = $address->getCountryCode();
    } else {
      // Case: custom information
      $billing_profile = $this->order->getBillingProfile();
      $data['buyerPhone'] = $billing_profile->get('field_phone_number')->value;
      $data['buyerName'] = $billing_profile->get('field_full_name')->value;

      $buyer_address =  $billing_profile->get('field_address')->value;
      if (is_null($buyer_address)) {
        $buyer_address = 'Cửa hàng Edigi - 02 Công Xã Paris, P. Bến Nghé, Q.1';
      }
      $buyer_city = $billing_profile->get('field_province')->value;
      if (is_null($buyer_city)) {
        $buyer_city = 'TP Hồ Chí Minh';
      }
      $data['buyerAddress'] = $buyer_address;
      $data['buyerCity'] = $buyer_city;
      $data['buyerCountry'] = 'VN';
    }

    \Drupal::logger('commerce_alepay')->info('captureAlepayWallet() 0-DATA: ' . json_encode($data));
    return $data;
  }

  public function sendOrderToAlepay($data)
  {
    // get demo data
    // $data = $this->createCheckoutData();
    $data['returnUrl'] = $this->getReturnUrl();
    // $data['cancelUrl'] = 'http://' . $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . '/demo-alepay';
    $url = $this->baseURL[$this->env] . $this->URI['requestPayment'];
    $result = $this->sendRequestToAlepay($data, $url);

    if (isset($result) && $result->errorCode == '000') {
      $dataDecrypted = $this->alepayUtils->decryptData($result->data, $this->publicKey);
      \Drupal::logger('commerce_alepay')->info('sendOrderToAlepay() 2-RESULTS: ' . $dataDecrypted);
      return json_decode($dataDecrypted, TRUE);
    } else {
      return false;
    }
  }

  public function decryptCallbackData($data, $configuration)
  {
    $this->configuration = $configuration;
    $this->setConnectionCodePayment();
    $data_decrypted = $this->alepayUtils->decryptCallbackData($data, $this->publicKey);
    $data_decrypted = json_decode($data_decrypted, TRUE);
    return $data_decrypted;
  }

  /*
    * get transaction info from Alepay
    * @param array|null $data
    */
  public function getTransactionInfo($transactionCode)
  {
    $data = array('transactionCode' => $transactionCode);
    $url = $this->baseURL[$this->env] . $this->URI['getTransactionInfo'];
    $result = $this->sendRequestToAlepay($data, $url);
    if ('000' == $result->errorCode) {
      $dataDecrypted = $this->alepayUtils->decryptData($result->data, $this->publicKey);
      return $dataDecrypted;
    } else {
      return json_encode($result);
    }
  }

  /**
   * Get Url to redirect after payment.
   *
   * @return \Drupal\Core\GeneratedUrl|string
   *   Return url.
   */
  public function getReturnUrl()
  {
    return Url::FromRoute('commerce_payment.checkout.return', [
      'commerce_order' => $this->order->id(),
      'step' => 'payment',
    ], ['absolute' => TRUE])->toString();
  }

}