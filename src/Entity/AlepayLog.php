<?php

namespace Drupal\commerce_alepay\Entity;


class AlepayLog
{
  private $transactionLogDirectory = "private://transaction-logs";
  private $transactionLogName = "transaction_{ORDER_ID}.json";
  private $nameReplace = "{ORDER_ID}";

  /**
   * @param array $data
   * @return true/ false
   */
  public function writeTransactionToFile($data)
  {
    try {
      /**
       * TODO: need update code when drupal upgrade core
       * Current code is base on core drupal 8.6.12
       */
      $file_name = str_replace($this->nameReplace, $data['orderCode'], $this->transactionLogName);
      if (file_prepare_directory($this->transactionLogDirectory, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS)) {
        $output = (is_array($data)) ? json_encode($data, JSON_NUMERIC_CHECK) : $data;
        $fileLocation = $this->transactionLogDirectory . '/' . $file_name;

        if ($file = file_save_data($output, $fileLocation, FILE_EXISTS_REPLACE)) {
          \Drupal::logger('commerce_alepay_log')->info("LOGGED transaction: " . $data['orderCode'] . " payment fid: " . $file->id());
          return true;
        }
      }
    } catch (\Exception $ex) {
      \Drupal::logger('commerce_alepay_log')->error("writeTransactionToFile() ERROR MESSAGE: " . $ex->getMessage());
      return false;
    }
  }

  /**
   * @return array/ false
   */
  public function getContentTransactionLog($order_id)
  {
    $file_name = str_replace($this->nameReplace, $order_id, $this->transactionLogName);
    $path_file_log = $this->transactionLogDirectory . '/' . $file_name;
    $data = file_get_contents($path_file_log);
    if ($data) {
      return json_decode($data, true, 512, JSON_BIGINT_AS_STRING);
    }
    return $data;
  }

}