<?php

namespace Drupal\commerce_alepay\Entity\TransactionResponse;

/**
 * Class ResponseBase.
 *
 * @package Drupal\commerce_alepay\Entity\TransactionResponse
 */
abstract class ResponseBase {

  const SUCCESS = 000;

  /**
   * Response code from Alepay.
   *
   * @var int
   */
  protected $code;

  protected $error_code;

  /**
   * Message from Alepay.
   *
   * @var string
   */
  protected $message;

  protected $response;

  /**
   * ResponseBase constructor.
   *
   * @param array $response
   *   Response data from Alepay Api.
   */
  public function __construct(array $response) {

    $this->error_code = [
      '000' => 'Thành công',
      '101' => 'Checksum không hợp lệ',
      '102' => 'Mã hóa không hợp lệ',
      '103' => 'IP không được phép truy cập',
      '104' => 'Dữ liệu không hợp lệ',
      '105' => 'Token key không hợp lệ',
      '106' => 'Token thanh toán Alepay không tồn tại hoặc đã bị hủy',
      '107' => 'Giao dịch đang được xử lý',
      '108' => 'Dữ liệu không tìm thấy',
      '109' => 'Mã đơn hàng không tìm thấy',
      '110' => 'Phải có email hoặc số điện thoại người mua',
      '111' => 'Giao dịch thất bại',
      '120' => 'Giá trị đơn hàng phải lớn hơn 0',
      '121' => 'Loại tiền tệ không hợp lệ',
      '122' => 'Mô tả đơn hàng không tìm thấy',
      '123' => 'Tổng số sản phẩm phải lớn hơn không',
      '124' => 'Định dạng URL không chính xác (http://, https://)',
      '125' => 'Tên người mua không đúng định dạng',
      '126' => 'Email người mua không đúng định dạng',
      '127' => 'SĐT người mua không đúng định dạng',
      '128' => 'Địa chỉ người mua không hợp lệ',
      '129' => 'City người mua không hợp lệ',
      '130' => 'Quốc gia người mua không hợp lệ',
      '131' => 'Hạn thanh toán phải lớn hơn 0',
      '132' => 'Email không hợp lệ',
      '133' => 'Thông tin thẻ không hợp lệ',
      '134' => 'Thẻ hết hạn mức thanh toán',
      '135' => 'Giao dịch bị từ chối bởi ngân hàng phát hành thẻ',
      '136' => 'Mã giao dịch không tồn tại',
      '137' => 'Giao dịch không hợp lệ',
      '138' => 'Tài khoản Merchant không tồn tại',
      '139' => 'Tài khoản Merchant không hoạt động',
      '140' => 'Tài khoản Merchant không hợp lệ',
      '142' => 'Ngân hàng không hỗ trợ trả góp',
      '' => '',
      '999' => 'Lỗi không xác định. Vui lòng liên hệ với Quản trị viên Alepay'
    ];

    $this->code = $response['errorCode'];
    $this->message = $response['message'];
    $this->response = $response;
  }

  /**
   * Get code.
   *
   * @return int
   *   Return code.
   */
  public function getCode() {
    return $this->code;
  }

  /**
   * Get message.
   *
   * @return string
   *   Return message.
   */
  public function getMessage() {
    return $this->message;
  }

}
