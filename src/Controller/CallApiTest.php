<?php

namespace Drupal\commerce_alepay\Controller;


use Drupal\commerce_alepay\Entity\Alepay;
use Drupal\commerce_alepay\Entity\AlepayLog;
use Drupal\commerce_alepay\Entity\AlepayUtils;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class CallApiTest
{
  public function requestOrder()
  {
    $host = \Drupal::request()->getSchemeAndHttpHost();
    $configAlepay = array(
      "apiKey" => "MRyQPU8VoxyFLVP6jrsJtxSBsn19v5",
      "encryptKey" => "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCn6oG39ETO/cC7NjT1MvR2r51pM8N9x/u32ofaW/1Q04dmIYcekj3a8dXrG5ry9mhZVIicOKyCFu2SMaWuG+yIiRpRbgAEiI6kB0dOK8+elc+ylSYwVpKWiGTdd4/sPB1UNxG3D2CoMgNT4RufnxfUWlL4heAO9wfutZptBtnc6QIDAQAB",
      "checksumKey" => "Jljjk5jK0sqlpUXj8akDseQ9oFt3Us",
      "callbackUrl" => $host . '/callback-api/alepay', // Đường dẫn sẽ xử lý khi thanh toán xong.
      "env" => "test", // or live
    );

    $data['cancelUrl'] = $host . '/callback-api/alepay';
    $data['returnUrl'] = $host . '/callback-api/alepay';
    $data['orderCode'] = 'C9XPnEnT';
    $data['amount'] = 3000000.000;
    $data['currency'] = 'VND';
    $data['orderDescription'] = 'Test order';
    $data['totalItem'] = 1;
    $data['checkoutType'] = 1; // 0 : cho phép thanh tán bằng cả 2 cách, 1 : chỉ thanh toán thường , 2: chỉ thanh toán trả góp
    $data['buyerName'] = 'Test';
    $data['buyerEmail'] = 'trananhtri1994@gmail.com';
    $data['buyerPhone'] = '0962945424';
    $data['buyerAddress'] = '294 Trường Sa';
    $data['buyerCity'] = 'TP Hồ Chí Minh';
    $data['buyerCountry'] = 'Vietnam';
    $data['paymentHours'] = '12'; // 12 tiếng :  Thời gian cho phép thanh toán (tính bằng giờ)

    $alepay = new Alepay($configAlepay);
    $result = $alepay->sendOrderToAlepay($data); // Khởi tạo
    dump($result);
    return new JsonResponse($result);
  }

  public function requestTransactionInfo($transation_code)
  {
    $alepay_log = new AlepayLog();
    $data = $alepay_log->getContentTransactionLog($transation_code);
    return new JsonResponse($data);

    $alepayUtils = new AlepayUtils();
    $publicKey = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCn6oG39ETO/cC7NjT1MvR2r51pM8N9x/u32ofaW/1Q04dmIYcekj3a8dXrG5ry9mhZVIicOKyCFu2SMaWuG+yIiRpRbgAEiI6kB0dOK8+elc+ylSYwVpKWiGTdd4/sPB1UNxG3D2CoMgNT4RufnxfUWlL4heAO9wfutZptBtnc6QIDAQAB";
    $dataDecrypted = $alepayUtils->decryptData($transation_code, $publicKey);
    dump($dataDecrypted);
    return new JsonResponse($dataDecrypted);
  }

  public function callBackReturn(Request $request)
  {
    $response = $request->query->all();
    return new JsonResponse($response);
  }

}