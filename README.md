CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Maintainers

Introduction
------------

This module integrate Alepay to Commerce Payment system

Requirements
------------

This module requires the following modules:

 * Drupal Commerce (https://www.drupal.org/project/commerce)


Installation
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.


Maintainers
-----------

 * Tri Tran 
 
 * Lap Pham (phthlaap) - https://www.drupal.org/user/3579545
